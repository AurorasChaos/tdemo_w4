﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class CorruptionZone : MonoBehaviour
{
    public PlayerController playerController;
    // Start is called before the first frame update

//    private void OnCollisionEnter2D(Collision2D collision)
/*
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.name == "Krieger") 
        {
            playerController.ModifyCorruption(1);
        }

    }
*/
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Krieger")
        {
            playerController.inCorruptionZone = true;
        }

    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name == "Krieger")
        {
            playerController.inCorruptionZone = false;
        }

    }

}
