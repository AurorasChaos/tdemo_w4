﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class enemyCode : MonoBehaviour
{
    private Rigidbody2D playerRigidbody2D;
    public Transform ENattackPos;
    public float attackRange;
    public float attackStart;
    private float betweenAttack;
    public LayerMask enemyDetector;
    public int health;
    public int enDamage;


    void Update()
    {
        if (betweenAttack <= 0)
        {

         Collider2D[] enemiesToHit = Physics2D.OverlapCircleAll(ENattackPos.position, attackRange, enemyDetector);
        for (int i = 0; i < enemiesToHit.Length; i++){
//        enemiesToHit[i].GetComponent<KriegMovement>().TakeDamege(enDamage);
        enemiesToHit[i].GetComponent<KriegMovement>().playerController.ModifyHealth(-enDamage);
        }
   
            betweenAttack = attackStart;
        }
        else
        {
            betweenAttack -= Time.deltaTime;
        }
        if (health <= 0)
        {
            Destroy(gameObject);

        }



        if (health <= 0)
        {
            Destroy(gameObject);

        }
    }
    public void TakeDamege(int damage)
    {
        health -= damage;
        UnityEngine.Debug.Log("damage taken");
    }

    void OnDrawGizmosSelected()
    {
     Gizmos.color = Color.red;
     Gizmos.DrawWireSphere(ENattackPos.position, attackRange);
    }

}

