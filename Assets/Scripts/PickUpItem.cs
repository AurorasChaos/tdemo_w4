﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItem : MonoBehaviour
{
    public PlayerController playerController;
    public EquipmentObject storedItem;
    public Sprite itemIcon;
//    public SpriteRenderer spriteRenderer;

    void Start()
    {
//        itemIcon = storedItem.equipmentIcon;
//          ChangeSprite(storedItem.equipmentIcon);
//        spriteRenderer.sprite = storedItem.equipmentIcon;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Krieger")
        {
            playerController.equipmentPickUp = true;
            playerController.currentPickUp = this;
        }

    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name == "Krieger")
        {
            playerController.equipmentPickUp = false;
        }

    }

}
