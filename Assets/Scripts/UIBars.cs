﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBars : MonoBehaviour
{
    public Image healthBar;
    public Image moraleBar;
    public Image corruptionBar;
    public float healthStartingY;
    public float moraleStartingY;
    public float corruptionStartingY;
    public int tempVariable = 1;
    public PlayerController playerController; 

    // Start is called before the first frame update
    void Start()
    {
        Vector2 healthBarSize = healthBar.rectTransform.sizeDelta;
        healthStartingY = healthBarSize.y;
        Vector2 moraleBarSize = healthBar.rectTransform.sizeDelta;
        moraleStartingY = moraleBarSize.y;
        Vector2 corruptionBarSize = healthBar.rectTransform.sizeDelta;
        corruptionStartingY = corruptionBarSize.y;
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.rectTransform.sizeDelta = new Vector2(playerController.health / 5, healthStartingY);
        moraleBar.rectTransform.sizeDelta = new Vector2(playerController.morale / 5, moraleStartingY);
        corruptionBar.rectTransform.sizeDelta = new Vector2(playerController.corruption / 5, corruptionStartingY);
    }
}
