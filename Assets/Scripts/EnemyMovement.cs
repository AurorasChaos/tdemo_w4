﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float enemyMoveSpeed;
    public float enemyJumpHeight;
    public Rigidbody2D enemyRigidbody2D;

    public LayerMask playerMask;

    public float aggroRange;

    private bool isJumping;

    public Transform target;

    void Update()
    {
        MoveEnemy();
    }

    void MoveEnemy()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, (target.position - transform.position).normalized, aggroRange, playerMask);
        if (hit.collider != null && target.gameObject.GetComponent<PlayerController>().alive)
        {
            if (target.position.x + 0.1f < transform.position.x)
            {
                Vector3 movement = new Vector3(-1f, 0f, 0f);
                transform.position += movement * Time.deltaTime * enemyMoveSpeed;
            }
            if (target.position.x - 0.1f > transform.position.x)
            {
                Vector3 movement = new Vector3(1f, 0f, 0f);
                transform.position += movement * Time.deltaTime * enemyMoveSpeed;
            }

            if (target.position.y > transform.position.y + 1f)
            {
                if (!isJumping)
                {
                    enemyJump();
                }
            }
        }
    }

    void enemyJump()
    {
        isJumping = true;
        enemyRigidbody2D.AddForce(Vector2.up * enemyJumpHeight);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            isJumping = false;
        }
    }
}
