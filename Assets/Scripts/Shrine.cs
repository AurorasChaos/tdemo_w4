﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shrine : MonoBehaviour
{
    public PlayerController playerController;

    public bool shrineUsed;
    
    void Start()
    {
        shrineUsed = false;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Krieger")
        {
            playerController.atShrine = true;
            playerController.currentShrine = this;
        }

    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name == "Krieger")
        {
            playerController.atShrine=false;
        }

    }
}
